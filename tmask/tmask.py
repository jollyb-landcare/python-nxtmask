# -*- coding: utf-8 -*-
"""
Created on Mon Sep 12 14:20:25 2016

@author: jollyb

Implement the Tmask Landsat cloud method, as per
    Zhu, Z. and Woodcock, C. (2014). Automated cloud, cloud shadow, and snow detection
        in multitemporal Landsat data: An algorithm designed specifically for monitoring 
        land cover change. Remote Sensing of Environment 152, pp 217-234. 


This is based on a quick hack by Neil Flood
"""

import numpy as np
from scipy.interpolate import griddata
from scipy import ndimage
from robreg import robustregression

def _generate_independents(juldates, daysperyear=365, nyears=3):
    
    # Set up the independant variables for the robust regression. These are functions of date. 
    constant = np.ones(juldates.shape)
    cosT = np.cos(2.0 * np.pi * juldates / daysperyear)
    sinT = np.sin(2.0 * np.pi * juldates / daysperyear)
    cosNT = np.cos(2.0 * np.pi * juldates / (daysperyear * nyears))
    sinNT = np.sin(2.0 * np.pi * juldates / (daysperyear * nyears))
    
    return np.array([constant, cosT, sinT, cosNT, sinNT])
        
    
def calculate_coefficients(toa_ref, masks, juldates, toa_scale, nodata, MASK_CLOUD=2, MASK_SHADOW=3, MASK_SNOW=4, debug=False, interp=False):

    #save nodata locations that occur more than twice to mask final result (fix border problems)
    # note minimum level set to '2' to remove random dud pixels
    any_nodata = (toa_ref == nodata).all(axis=1).sum(axis=0) > 2
    imgx, imgy = any_nodata.shape
    imgsz = imgx * imgy
    print('TOA Shape: {0} x {1} = {2} pixels ({3} nodata)'.format(imgx, imgy, imgsz, any_nodata.sum()))
    
    scale_ref = toa_ref * toa_scale

    scale_nodata = nodata * toa_scale   
    
    num_dates, num_bands, num_rows, num_cols = scale_ref.shape
    
    #fmaskArray = np.array([c[0] for c in masks])
    cloud = (masks == MASK_CLOUD)
    shadow = (masks == MASK_SHADOW)
    snow = np.zeros_like(cloud)#(masks == MASK_SNOW)
    
    # Reflectance is null if any of the bands is null
    #refIsNull = np.array([(ref == scale_nodata).any(axis=0) for ref in scale_ref])
    refIsNull = (scale_ref == scale_nodata).all(axis=1)

    # Only use pixels that have at least 8 valid observations if enough images are available
    #minDates = 20
    minDates = min(8, scale_ref.shape[0])

    tooFewDates = (np.logical_not(refIsNull | cloud | shadow | snow).sum(axis=0) < minDates)
    tooFewDatesStack = np.expand_dims(tooFewDates, axis=0).repeat(num_dates, axis=0)
    
    totalNullMask = (refIsNull | cloud | shadow | snow | tooFewDatesStack)

    #import pdb; pdb.set_trace()
    
    # Now mask out anything which was null in input, or is detected as cloud/shadow/snow already
    for i in range(num_bands):
        scale_ref[:, i, :, :][totalNullMask] = scale_nodata
    
    # Fit the reflectance model for each band. This is as described in sections 3.3.1 & 3.3.2
    
    x = _generate_independents(juldates)
    num_params = len(x)
    
    #regression_data = []
    # Now fit for each band. The c array is the coeefficients of the fits. 
    rsq = np.zeros((num_bands, num_rows, num_cols), dtype=np.float32)
    numval = np.logical_not(refIsNull | cloud | shadow | snow).sum(axis=0)
    coeff = np.zeros((num_bands, num_params, num_rows, num_cols), dtype=np.float32)
    coeff_interp = None
    
    if interp:
        coeff_interp = np.zeros_like(coeff) + np.nan
        gx, gy = np.mgrid[:imgx, :imgy] 
        
    for iband in range(num_bands):
        print('Fitting band %d' %iband)
        y = scale_ref[:, iband, :, :]
        y[totalNullMask] = scale_nodata
        #import pdb; pdb.set_trace()
        regObj = robustregression.gsl_multifit_robust(x, y, method=robustregression.GSL_METHOD_BISQUARE,#HUBER,#OLS,#BISQUARE,
            nullVal=scale_nodata)
            
        #regression_data.append(regObj)
            
        coeff[iband, :, :, :] = regObj.coeffs
        #import pdb; pdb.set_trace()
        rsq[iband] = regObj.Rsqrd
        
        if interp:
            nul = (regObj.coeffs == 0).any(axis=0)
            numnul = nul.sum()
            #only interpolate if some pixels actually need it, we need at least 10 non-nul to make it worth our time
            if 0 < numnul < (imgsz - 10):
                cx, cy = gx[~nul], gy[~nul]
                for iparam in range(num_params):
                    c = regObj.coeffs[iparam, ~nul]
                    coeff_interp[iband, iparam] = griddata((cx, cy), c, (gx, gy), method='cubic')
            else:
                coeff_interp[iband, :, :, :] = regObj.coeffs
                        
        
        #if debug and (regObj.numIter == 0).any():
        #    import pdb; pdb.set_trace()

    coeff[:, :, any_nodata] = 0
    
    if interp:
        allnul = (coeff == 0).all(axis=0).all(axis=0)
        allnul_clumps = ndimage.label(allnul, np.array([[0, 1, 0], [1,1,1], [0,1,0]]))[0]
        allnul_counts = np.bincount(allnul_clumps.flatten())
        for i, count in enumerate(allnul_counts):
            if count < 100:
                allnul[allnul_clumps == i] = False
        #import pdb; pdb.set_trace()
        coeff_interp[:, :, allnul | any_nodata] = 0
    
    #import pdb; pdb.set_trace()
    return coeff, numval, any_nodata, rsq, coeff_interp, scale_ref / toa_scale
    
def model_toa(coeff, juldates):
    
    num_bands, num_params, num_rows, num_cols = coeff.shape
    num_dates = len(juldates)
    
    x = _generate_independents(juldates)
    
    # Now do Tmask spectral differencing, as per section 3.3.3    
    refModelled = np.zeros((num_dates, num_bands, num_rows, num_cols), dtype=np.float32)
    for bandNdx in range(num_bands):
        for d in range(num_dates):
            for param in range(num_params):
                refModelled[d, bandNdx, :, :] += coeff[bandNdx, param] * x[param, d]
   
    return refModelled
    

def generate_masks(toa_ref, coeff, juldates, toa_scale, nodata_ref, nodata_coeff, modelled=None, MASK_NULL=0, MASK_CLEAR=1, MASK_CLOUD=2, MASK_SHADOW=3, MASK_SNOW=4):
    # This is the value to use masked things in the new output masks
    IS_MASK = 2
    
    # Array index values for each relevant band
    TM2, TM4, TM5 = 0, 1, 2
    
    num_bands, num_params, num_rows, num_cols = coeff.shape
    num_dates = len(juldates)
            
    scale_ref = toa_ref.copy() * toa_scale

    scale_nodata = nodata_ref * toa_scale

    nul_coeffs = (coeff == nodata_coeff).all(axis=0).all(axis=0)

    if modelled is None:
        modelled = model_toa(coeff, juldates)
    
    # Reflectance is null if any of the bands is null
    #refIsNull = np.array([(ref == scale_nodata).any(axis=0) for ref in scale_ref])
    refIsNull = (scale_ref == scale_nodata).all(axis=1)

    # Only use pixels that have at least 30 valid observations if enough images are available
    #minDates = 20
    minDates = len(scale_ref) if len(scale_ref) < 30 else 30

    tooFewDates = (np.logical_not(refIsNull).sum(axis=0)  < minDates)
    tooFewDatesStack = np.expand_dims(tooFewDates, axis=0).repeat(num_dates, axis=0)
    
    # Difference between modelled and observed. 
    ref_diff = scale_ref - modelled
    
    # Start the outputs with 1 (clear) everywhere
    outcloud = np.ones((num_dates, num_rows, num_cols), dtype=np.uint8)
    outshadow = np.ones((num_dates, num_rows, num_cols), dtype=np.uint8)
    outsnow = np.ones((num_dates, num_rows, num_cols), dtype=np.uint8)
    
    # Now add extra bits as determined by the Tmask algorithm
    tm2GT04 = (ref_diff[:, TM2, :, :] > 0.04)
    tm4GT04 = (ref_diff[:, TM4, :, :] > 0.04)
    # Leaving out the snow tests for now. ...... come back to this......
    
    tm4LTn04 = (ref_diff[:, TM4, :, :] < -0.04)
    tm5LTn04 = (ref_diff[:, TM5, :, :] < -0.04)
    
    outcloud[tm2GT04] = IS_MASK     # Left out the snow option
    outshadow[(~tm2GT04) & tm4LTn04 & tm5LTn04] = IS_MASK
    # Leaving out the snow tests
    
    # Set masks to null wherever the reflectance is null
    outcloud[refIsNull] = 0
    outshadow[refIsNull] = 0
    outsnow[refIsNull] = 0
    
    # Combine as single layers for each date, since that is how other people seem to like it.
    # Start with the snow, and over-write with shadow then cloud. 
    outimg = np.zeros(outcloud.shape, dtype=np.uint8)
    outimg.fill(MASK_CLEAR)
    outimg[outsnow == IS_MASK] = MASK_SNOW
    outimg[outshadow == IS_MASK] = MASK_SHADOW
    outimg[outcloud == IS_MASK] = MASK_CLOUD
    outimg[refIsNull] = MASK_NULL
    outimg[tooFewDatesStack] = MASK_NULL
    outimg[:, nul_coeffs] = MASK_NULL

    return outimg

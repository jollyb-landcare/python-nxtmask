import os, glob
from distutils.core import setup

cmd = 'make -C robreg/'
print(cmd)
os.system(cmd)

packages = ["tmask", "robreg"]

if os.environ.get("INSTALL_LCRFS") is not None:
    print("Installing lcrfs as per commandline args")
    packages.append("lcrfs")
    
robreg_so = glob.glob('robreg/robreg.*.so')
print(robreg_so)
assert len(robreg_so) == 1, 'Expected only one *.so in robreg/'
robreg_so = os.path.basename(robreg_so[0])

setup(
    name = "nxtmask",
    packages = packages,
    package_data = {'robreg': [robreg_so]},
    scripts = [
        "bin/nxtmask.py",
        "bin/nxtmask_gencoeff.py",
        "bin/nxtmask_genmask.py"
    ],
    version = "1.0.5",
    description = "Landcare tmask implementation based on Neil Flood's implementation",
    author = "Ben Jolly",
    author_email = "jollyb@landcareresearch.co.nz",
    url = "https://bitbucket.org/jollyb-landcare/python-nxtmask",
    download_url = "",
    keywords = ["cloud", "satellite", "landcare"],
    classifiers = [
        "Programming Language :: Python :: 3",
        "Development Status :: 5 - Production",
        "Environment :: Other Environment",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules",
        ],
    long_description = """\
This version requires Python 3 or later.
"""
)

#!/usr/bin/env python
"""
Created on Fri Sep 16 13:31:14 2016

@author: jollyb
"""


import argparse
import os
import lcrfs

p = argparse.ArgumentParser()
p.add_argument("coefficients", help="File holding coefficients")
p.add_argument("reflist", nargs='+', help="TOA reflectance files")
p.add_argument("--outlist", nargs='+', default=None, help="Corresponding output files for tmasks (optional)")
p.add_argument("--outdir", default=None, help="Corresponding directory for output files (optional, overrides dirs in --outlist)")
p.add_argument("--numthreads", type=int, default=1, help="Number of threads to use (RIOS) - Default=%(default)s")
p.add_argument("--bands", type=int, nargs=3, default=[3, 5, 6], help="Images bands to use for (in order): Green, NIR, SWIR (default LS8 - [3, 5, 6])")

p.add_argument("--savemod", action="store_true", help="Save modelled (predicted) images")
p.add_argument("--debug", action="store_true", help="Debug flag")
p.add_argument("--nodata", type=int, default=0, help="nodata value for inputs (int, default 0)")

args = p.parse_args()
    
if args.outlist is None:
    args.outlist = [lcrfs.change(os.path.splitext(x)[0] + '.kea', 'stage', 'tmask') for x in args.reflist]

if args.outdir is not None:
    args.outlist = [os.path.join(args.outdir, os.path.basename(x)) for x in args.outlist]    
    
extra_flags = '--nodata {} {} {}'.format(args.nodata, '--savemod' if args.savemod else '',  ' --debug' if args.debug else '')
cmd = 'nxtmask.py {coeff} {reflist} --outlist {outlist} --genmasks --numthreads {numthreads} --bands {bands} {extra_flags}'.format(
    coeff = args.coefficients,
    reflist = ' '.join(args.reflist),
    outlist = ' '.join(args.outlist),
    numthreads = args.numthreads,
    bands = ' '.join(str(x) for x in args.bands),
    extra_flags = extra_flags
)
print(cmd)
os.system(cmd)

#!/usr/bin/env python
"""
Created on Fri Sep 16 13:31:14 2016

@author: jollyb
"""


import argparse
import os
import lcrfs

p = argparse.ArgumentParser()
p.add_argument("coefficients", help="File holding coefficients")
p.add_argument("reflist", nargs='+', help="TOA reflectance files")
p.add_argument("--cloudlist", nargs='+', default=None, help="Corresponding fmask cloud cmasks for TOA")
p.add_argument("--numthreads", type=int, default=1, help="Number of threads to use (RIOS) - Default=%(default)s")
p.add_argument("--bands", type=int, nargs=3, default=[3, 5, 6], help="Images bands to use for (in order): Green, NIR, SWIR (default LS8 - [3, 5, 6])")
p.add_argument("--refimg", default=None)
p.add_argument("--rawpxlist", nargs='+', default=None, help="List of three files for raw input pixels (only works with --debug)")
p.add_argument("--debug", action="store_true", help="Debug flag")
p.add_argument("--nodata", type=int, default=0, help="nodata value for inputs (int, default 0)")
args = p.parse_args()
    
if args.cloudlist is None:
    args.cloudlist = [lcrfs.change(x, 'stage', 'cloud') for x in args.reflist]

extra_flags = '--nodata {}'.format(args.nodata)

if args.debug:
    extra_flags += ' --debug'
    if args.rawpxlist is not None:    
        extra_flags += ' --rawpxlist ' + ' '.join(args.rawpixlist)

if args.refimg is not None:
    extra_flags += ' --refimg ' + args.refimg
        
cmd = 'nxtmask.py {coeff} {reflist} --cloudlist {cloudlist} --gencoeff --numthreads {numthreads} --bands {bands} {extra_flags}'.format(
    coeff = args.coefficients,
    reflist = ' '.join(args.reflist),
    cloudlist = ' '.join(args.cloudlist),
    numthreads = args.numthreads,
    bands = ' '.join(str(x) for x in args.bands),
    extra_flags = extra_flags
)
print(cmd)
os.system(cmd)

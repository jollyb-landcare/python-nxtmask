#!/usr/bin/env python
"""
Based on Neil Flood's tmask hack
"""

import argparse
import time
import os

import numpy as np
from scipy.interpolate import griddata

from rios import applier, cuiprogress, rat

import dateutil.parser as dparse

from tmask import tmask

haveGDALPy = True
try:
    import osgeo.gdal as gdal
except ImportError as gdalErr:
    haveGDALPy = False

# Constants for coding cloud, shadow, snow and null in the masks, both for input
# Fmask and output Tmask layers. 
MASK_NULL = 0
MASK_CLEAR = 1
MASK_CLOUD = 2
MASK_SHADOW = 3
MASK_SNOW = 4


def getCmdargs():
    """
    Get commandline arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument("coefficients", help="File holding coefficients")
    p.add_argument("reflist", nargs='+', help="TOA reflectance files")
    p.add_argument("--cloudlist", nargs='+', default=None, help="Corresponding fmask cloud cmasks for TOA")
    p.add_argument("--outlist", nargs='+', default=None, help="Corresponding output files for tmasks")
    #p.add_argument("--rawpxlist", nargs='+', default=None, help="List of three files for raw input pixels")
    
    p.add_argument("--numthreads", type=int, default=1, help="Number of threads to use (RIOS) - Default=%(default)s")
    p.add_argument("--bands", type=int, nargs=3, default=[3, 5, 6], help="Images bands to use for (in order): Green, NIR, SWIR (default LS8 - [3, 5, 6])")
    p.add_argument('--refimg', default=None)

    p.add_argument("--gencoeff", action="store_true", help="Generate new coefficient files")
    p.add_argument("--genmasks", action="store_true", help="Generate tmask masks")
    p.add_argument("--savemod", action="store_true", help="Save modelled (predicted) images")
    p.add_argument("--debug", action="store_true", help="Debug flag")
    p.add_argument("--nodata", type=int, default=0, help="nodata value for inputs (int, default 0)")
    args = p.parse_args()
    return args

def date_to_julian_day(my_date):
    """Returns the Julian day number of a date."""
    a = (14 - my_date.month)//12
    y = my_date.year + 4800 - a
    m = my_date.month + 12*a - 3
    return my_date.day + ((153*m + 2)//5) + 365*y + y//4 - y//100 + y//400 - 32045
    
def mainRoutine():
    """
    Main routine
    """
    args = getCmdargs()
    #numthreads = cmdargs.numthreads
    
    #(reflist, cloudlist, datelist, outlist, coeffpath) = readInputs(cmdargs)
    #nxtmask(reflist, cloudlist, datelist, outlist, coeffpath=coeffpath)
    nxtmask(
        args.reflist, args.bands, args.coefficients, 
        args.gencoeff, args.genmasks, 
        outlist=args.outlist, cloudlist=args.cloudlist, #rawpxlist=args.rawpxlist,
        numthreads=args.numthreads, save_modelled=args.savemod, debug=args.debug, refimg=args.refimg, nodata=args.nodata
    )
    

def nxtmask(
        reflist, bands, coefficients, 
        gen_coeff, gen_masks, 
        outlist=None, cloudlist=None, #rawpxlist=None,
        numthreads=1, save_modelled=False, debug=False, refimg=None, nodata=0
    ):   

    infiles = applier.FilenameAssociations()
    outfiles = applier.FilenameAssociations()
    otherargs = applier.OtherInputs()
    controls = applier.ApplierControls()
       
    infiles.reflectance = reflist
    
    datelist = [dparse.parse(os.path.basename(x), fuzzy=True) for x in reflist]
    
    if gen_coeff:
        assert not gen_masks, "Currently unable to generate coefficients and masks in the same instance"
        assert (cloudlist is not None) and (len(cloudlist) == len(reflist)), "--cloudlist must be supplied and the same length as reflist if using --gen_coeff"
        infiles.fmasks = cloudlist
        outfiles.coefficients = coefficients
        outfiles.interpcoefficients = os.path.splitext(coefficients)
        outfiles.interpcoefficients = '_interp'.join(outfiles.interpcoefficients)
        
        # Need to use UNION, because there are a few "short" images, which 
        # chop the intersection down radically. 
        controls.setFootprintType(applier.UNION)
        
        coeff_band_names = []
        for a in ['const', 'sin', 'cos', 'sinNT', 'cosNT']:
            for i, b in enumerate(['GN', 'NIR', 'SWIR']):
                coeff_band_names.append('{0}_{1}_{2}'.format(a, b, bands[i]))

        """
        coeff_band_names = [
            'const_GN_3', 'const_NIR_5', 'const_SWIR_6',
            'sin_GN_3', 'sin_NIR_5', 'sin_SWIR_6',
            'cos_GN_3', 'cos_NIR_5', 'cos_SWIR_6',
            'sinNT_GN_3', 'sinNT_NIR_5', 'sinNT_SWIR_6',
            'cosNT_GN_3', 'cosNT_NIR_5', 'cosNT_SWIR_6'
        ]
        """
        
        def coeff_controls(imagename):
            controls.setLayerNames(coeff_band_names, imagename=imagename)
            controls.setStatsIgnore(0, imagename=imagename)
            controls.setCalcStats(True, imagename=imagename)
        
        coeff_controls('coefficients')
        coeff_controls('interpcoefficients')


        if debug:
            outfiles.debug = os.path.splitext(coefficients)
            outfiles.debug = '_DEBUG'.join(outfiles.debug)

        
    else:
        infiles.coefficients = coefficients
        
        # Need to set reference image as coeficients file potentially much larger
        # than input images so output needs to be clipped
        controls.setReferenceImage(infiles.reflectance[0])
        
        
    if gen_masks:
        assert not gen_coeff, "Currently unable to generate coefficients and masks in the same instance"
        assert (outlist is not None) and (len(outlist) == len(reflist)), "--outlist must be supplied and the same length as reflist if using --gen_masks"
        outfiles.tmasks = outlist
        controls.setThematic(True, imagename='tmasks')
        
    if save_modelled:
        outfiles.modelled = [os.path.splitext(x) for x in outlist]
        outfiles.modelled = [x[0] + '_modelled' + x[1] for x in outfiles.modelled]
        
    #if rawpxlist is not None:
    #    assert len(rawpxlist) == 3, "Three raw pixel files are required (one for each band)"
    #    outfiles.rawpx = rawpxlist
    #    controls.setLayerNames([os.path.basename(x).split('_')[-3] for x in reflist], imagename='rawpx')
        
    #controls.setProgress(cuiprogress.GDALProgressBar())       

    if refimg is not None:
        controls.setReferenceImage(refimg)
    
    controls.setWindowXsize(512)
    controls.setWindowYsize(512)
    
    # Parallel processing, for speed, if requested
    controls.setNumThreads(numthreads)
    
    # Band selection
    assert type(bands) is list and len(bands) == 3, "Three band numbers are required (Green, NIR, SWIR)"
    if debug: print("Using bands:", bands)
    
    controls.selectInputImageLayers(bands, imagename='reflectance')

    """
    # Tmask only uses TM bands 2, 4 and 5
    if sat_type == 'landsat8':    
        # Corresponds to the following Landsat 8 bands
        layerSelection = [3, 5, 6]
    else:
        layerSelection = [2, 4, 5]            
    """
    
    try:
        controls.tempdir = os.environ["TMPDIR"]
    except:
        if numthreads > 1:
            print('WARNING: couldn\'t set tmp directory (check TMPDIR env var is set)')
        else:
            pass
   
    otherargs.multiref = len(reflist) > 1
    otherargs.nodata = nodata #applier.OtherInputs() #just need an empty object and this one will do nicely
    otherargs.juldates = np.array([date_to_julian_day(date) for date in datelist])
    otherargs.ref_scale = 0.0001
    
    otherargs.gen_coeff = gen_coeff
    otherargs.gen_masks = gen_masks
    otherargs.save_modelled = save_modelled
    #otherargs.save_raw = rawpxlist is not None
    otherargs.debug = debug
    
    print('Run apply...')
    applier.apply(do_tmask, infiles, outfiles, otherargs, controls=controls)
    print('Done, do band names...')

    if gen_masks:
        # Add colour tables to all output files. 
        addColourTables(outfiles.tmasks)
       
    print('All Done')

def do_tmask(info, inputs, outputs, otherargs):
    """
    Called from RIOS
    
    Do the Tmask model and product output cloud masks for all the same dates as input. 
    
    """
    
    #if not hasattr(otherargs.nodata, 'reflectance'):
    #    #ref = inputs.reflectance[0] if otherargs.multiref else inputs.reflectance
    #    otherargs.nodata.reflectance = 0#info.getNoDataValueFor(ref)
       
    toa_ref = np.array(inputs.reflectance)
    #import pdb; pdb.set_trace()
    coeff = None    
    if otherargs.gen_coeff:
        if otherargs.debug: print('Gen coeff')
        fmasks = np.array([c[0] for c in inputs.fmasks])
                
        coeff, numval, any_nodata, rsq, coeff_interp, toa_used = tmask.calculate_coefficients(
            toa_ref, fmasks, otherargs.juldates, otherargs.ref_scale, 
            otherargs.nodata, debug=otherargs.debug, interp=True
        )
                
        #import pdb; pdb.set_trace() 
        
        if otherargs.debug: print('Save coeff', coeff.sum())
        outputs.coefficients = np.vstack([coeff[:,i,:,:] for i in range(coeff.shape[1])])
        outputs.interpcoefficients = np.vstack([coeff_interp[:,i,:,:] for i in range(coeff_interp.shape[1])])
        if otherargs.debug: print('\t', outputs.coefficients.shape)
        
        if otherargs.debug:
            print('Save debug')
            outputs.debug = np.vstack((any_nodata[np.newaxis, :, :], numval[np.newaxis, :, :], rsq))
            print('\t', outputs.debug.shape)
            
            #if otherargs.save_raw:
            #    print('Save rawpx')
            #    outputs.rawpx = [toa_ref[:,i,:,:] for i in range(rawpx.shape[1])]
    else:
        num_bands, num_params = 3, 5
        if otherargs.debug: print('Load coeff')
        coeff = np.array([inputs.coefficients[i:i+3] for i in range(0, num_bands*num_params, num_bands)]).swapaxes(0,1)
        if otherargs.debug: print('\t', coeff.shape)
      
    if otherargs.debug: print('Model refl')
    modelled = tmask.model_toa(coeff, otherargs.juldates)
    if otherargs.save_modelled:
        if otherargs.debug: print('Save modelled')
        outputs.modelled = [np.array([img])[0] for img in modelled]
        #print('\t', outputs.modelled.shape)

    if otherargs.gen_masks:
        if otherargs.debug: print('Gen masks')
        tmasks = tmask.generate_masks(
            toa_ref, coeff, 
            otherargs.juldates, otherargs.ref_scale, 
            otherargs.nodata, otherargs.nodata,  
            modelled=modelled
        )
        
        if otherargs.debug: print('Save masks')
        outputs.tmasks = [np.array([img]) for img in tmasks]
        #print('\t', outputs.tmasks.shape)

    if otherargs.debug: print('Done')

def addColourTables(outfiles):
    """
    Add colour tables to all the output files. We are given the list of
    output filenames, and we set the same colour table on all of them. 
    
    The colour tables are copied from the Fmask program, but have used slightly
    darker versions of the same colours, so I can see the difference on screen. 
    
    """
    clrTbl = np.array([
        [MASK_CLOUD, 200, 0, 200, 255],
        [MASK_SHADOW, 200, 200, 0, 255],
        [MASK_SNOW, 85, 200, 200, 255],
    ])
    
    for filename in outfiles:
        rat.setColorTable(filename, clrTbl)
        className = np.empty([256], dtype=np.dtype('a255'))
        className[...] = ""
        className[MASK_NULL] = "Undefined"
        className[MASK_CLEAR] = "Valid"
        className[MASK_CLOUD] = "Cloud"
        className[MASK_SHADOW] = "Cloud Shadow"
        className[MASK_SNOW] = "Snow"        
        rat.writeColumn(filename, "Class", className)
    

if __name__ == "__main__":
    start = time.time()
    print('nxtmask.py starting at:', start)
    
    mainRoutine()

    end = time.time()
    print('nxtmask.py ending at:', end)
    elapsed = end - start
    print('Elapsed time (tmask): %g seconds' %(elapsed))

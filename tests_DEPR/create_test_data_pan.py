#!/usr/bin/env python
"""
Creates test data for the test_landsatcloud_tmask.py unit tests
"""

import os
import glob
import argparse



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("testcase", type = str, help="one of standard, border")

    args = parser.parse_args()

    if args.testcase == 'standard':
        projwin = '1849600.0 5531760.0 1850560.0 5530800.0'
    elif args.testcase == 'border':
        projwin = '1766070.0 5597640.0 1767030.0 5596680.0'
    
    inpath = os.path.join(os.getenv('LCR_DATA'),'raster', 'landsat8')
    pathrow = 'p72r88'
    outpath = os.path.join('./testdata', args.testcase)

    if not os.path.exists('./testdata'):
        os.makedirs('./testdata')

    cloudlist = []
    toalist = []
    for year in range(2013, 2017):
        curpath = os.path.join(inpath, str(year), pathrow)
        cloudlist = cloudlist + glob.glob(curpath + '/landsat*re*cloud_nztm.kea')
        toalist = toalist + glob.glob(curpath + '/landsat*re*toa_nztm.kea')

    for cloudfile in cloudlist:
        outcloud = os.path.join(outpath, os.path.split(cloudfile)[1])
        cmd = 'gdal_translate -of kea -projwin {} {} {}'
        cmd = cmd.format(projwin, cloudfile, outcloud)
        #print(cmd)
        os.system(cmd)

    for toafile in toalist:
        outtoa = os.path.join(outpath, os.path.split(toafile)[1])
        cmd = 'gdal_translate -of kea -projwin {} {} {}'
        cmd = cmd.format(projwin, toafile, outtoa)
        #print(cmd)
        os.system(cmd)

#!/usr/bin/env python

"""
Test class for robreg.c and its python wrapper
"""

import sys
sys.path.append('./..')

import unittest
import numpy as np

import robreg
import robustregression


class WrapperWorks(unittest.TestCase):
    
    def test_wrap_gsl_multifit_robust(self):
        """
        Tests if the wrapped gsl_multifit_robust() method works by comparing the resulting coefficients
        to a set of predefined coefficients
        
        """
        x = np.array([[1, 0.9, 0.8], [0.8, 0.3, 0.5]])
        y = np.array([[[0.3, 0.2, 0.16], [0.2, 0.14, 0.15], [0.21, 0.23, 0.28]]
                      ,[[0.28, 0.21, 0.17], [0.2, 0.15, 0.18], [0.20, 0.26, 0.29]]
                      , [[0.7, 0.22, 0.19], [0.2, 0.16, 0.2], [0.21, 0.27, 0.29]]])
        
        method = 1 # = GSL_METHOD_BISQUARE
        (coeffs, adj_Rsqrd, numIter, rmse) = robreg.wrap_gsl_multifit_robust(x, y, method, 0, 0.0)

        test_coeffs = np.array([[[ 0.4122449,   0.26734694,  0.22040816],
                                 [ 0.24489796,  0.19387755,  0.24897959],
                                 [ 0.23877551,  0.34489796,  0.36326531]],
                                [[ 0.06938776, -0.06122449, -0.05102041],
                                 [-0.04081633, -0.04897959, -0.09387755],
                                 [-0.01836735, -0.1122449,  -0.07959184]]])

        test_adj_Rsqrd = np.array([[  -6.07719089,  -46.62369913,  -22.22222281],
                                   [         -np.inf,  -29.47916744,  -11.70800814],
                                   [ -82.97645613,  -19.52703544, -161.55555969]])

        test_numIter = np.array([[1, 1, 1],
                                 [1, 1, 1],
                                 [1, 1, 1]])

        test_rmse = np.array([[ 0.63029066,  0.06900993,  0.07361059],
                              [ 0.04600662,  0.05520794,  0.0897129 ],
                              [ 0.05290761,  0.09431357,  0.07361059]])

        #print(coeffs)
        #print(test_coeffs)

        self.assertTrue(np.allclose(coeffs, test_coeffs)) # Floating point equality means "close enough"
        self.assertTrue(np.allclose(adj_Rsqrd, test_adj_Rsqrd))
        self.assertTrue(np.allclose(numIter, test_numIter))
        self.assertTrue(np.allclose(rmse, test_rmse))
        

    def test_gsl_multifit_robust(self):
        """
        Tests if the gsl_multifit_robust() method works by comparing the resulting coefficients
        to a set of predefined coefficients
        
        """
        x = np.array([[1, 0.9, 0.8], [0.8, 0.3, 0.5]])
        y = np.array([[[0.3, 0.2, 0.16], [0.2, 0.14, 0.15], [0.21, 0.23, 0.28]]
                      ,[[0.28, 0.21, 0.17], [0.2, 0.15, 0.18], [0.20, 0.26, 0.29]]
                      , [[0.7, 0.22, 0.19], [0.2, 0.16, 0.2], [0.21, 0.27, 0.29]]])
        
        method = 1 # = GSL_METHOD_BISQUARE
        regObj = robustregression.gsl_multifit_robust(x, y, method, 0, 0.0)

        coeffs = regObj.coeffs
        adj_Rsqrd = regObj.adj_Rsqrd
        numIter = regObj.numIter
        rmse = regObj.rmse

        test_coeffs = np.array([[[ 0.4122449,   0.26734694,  0.22040816],
                                 [ 0.24489796,  0.19387755,  0.24897959],
                                 [ 0.23877551,  0.34489796,  0.36326531]],
                                [[ 0.06938776, -0.06122449, -0.05102041],
                                 [-0.04081633, -0.04897959, -0.09387755],
                                 [-0.01836735, -0.1122449,  -0.07959184]]])

        #print(coeffs)
        #print(test_coeffs)

        self.assertTrue(np.allclose(coeffs, test_coeffs)) # Floating point equality means "close enough"

if __name__ == '__main__':
    unittest.main()


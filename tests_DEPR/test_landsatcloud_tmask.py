#!/usr/bin/env python

# For dev purposes: make sure the module to be tested can be imported
import sys
sys.path.append('./..')
from landsatcloud_tmask import *
import unittest
import glob
import numpy as np
from osgeo import gdal

class KnownLS8Subset(unittest.TestCase):
    class Helper():        
        """
        Needed for testing readInputs
        """
        def __init__(self, testcase):
            if testcase == 'standard':
                self.reffilelist = './testdata/standard/reffiles.txt'
                self.cloudfilelist = './testdata/standard/cloudfiles.txt'
                self.datenumbers = './testdata/standard/datenumbers.txt'
                self.outfilelist = './testdata/standard/outfiles.txt'
            elif testcase == 'border':
                self.reffilelist = './testdata/border/reffiles.txt'
                self.cloudfilelist = './testdata/border/cloudfiles.txt'
                self.datenumbers = './testdata/border/datenumbers.txt'
                self.outfilelist = './testdata/border/outfiles.txt'


    def test_readInputs(self):
        """
        Not strictly necessary to test this method, but as it is used for test_tmask it seemed
        to make sense to make sure nothing goes wrong in the beginning.
        """
        cmdargs = self.Helper("standard")
                    
        reflist, cloudlist, datelist, outlist = readInputs(cmdargs)
        
        self.assertEqual(len(reflist), len(cloudlist))
        self.assertEqual(len(datelist), len(outlist))
        self.assertEqual(outlist[8], './testdata/standard/tmask_140117.kea')
        self.assertEqual(reflist[15], './testdata/standard/landsat8_olire_p72r88_140829_toa_nztm.kea')


    def test_tmask_standard(self):
        """
        Main test for TMASK; runs the algorithm on a 32x32 pixel landsat8 time series
        and checks the results
        """
        cmdargs = self.Helper("standard")
                    
        reflist, cloudlist, datelist, outlist = readInputs(cmdargs)
        #tmask(reflist, cloudlist, datelist, outlist, nodataval = 0)
        tmask(reflist, cloudlist, datelist, outlist)

        # Let's check...
        # ...1. the number of created output files
        tmasklist = glob.glob('./testdata/standard/tmask*.kea')
        tmasklist.sort()
        self.assertEqual(len(tmasklist), 36)
        
        # 2. values of three output files        
        ds1 = gdal.Open(tmasklist[1])
        ds2 = gdal.Open(tmasklist[16])
        ds3 = gdal.Open(tmasklist[24])
        arr1 = np.array(ds1.GetRasterBand(1).ReadAsArray())
        print(arr1[5:10, 5:10])
        print(np.mean(arr1))
        arr2 = np.array(ds2.GetRasterBand(1).ReadAsArray())
        print(arr2[5:10, 5:10])
        print(np.mean(arr2))
        arr3 = np.array(ds3.GetRasterBand(1).ReadAsArray())
        print(arr3[5:10, 5:10])
        print(np.mean(arr3))
        testarr1 = np.array([[1, 1, 1, 1, 1], [1, 1, 1, 1, 1,], [1, 1, 1, 1, 1], [1, 1, 1, 1, 1],
                             [1, 1, 1, 1, 1]])
        testarr2 = np.array([[2, 2, 2, 2, 2], [2, 2, 2, 2, 2], [2, 2, 2, 2, 2], [2, 2, 2, 2, 2],
                             [2, 2, 2, 2, 2]])
        testarr3 = np.array([[3, 3, 3, 1, 2], [3, 3, 3, 1, 2], [1, 3, 3, 1, 2], [1, 1, 2, 2, 2],
                             [1, 2, 2, 2, 2]])
        #import pdb; pdb.set_trace()
        self.assertTrue(abs(np.mean(arr1)-1.064453125) < 1e-09)
        
        self.assertTrue(np.array_equal(arr1[5:10, 5:10], testarr1))

        self.assertTrue(abs(np.mean(arr2)-2.044921875) < 1e-09)
        self.assertTrue(np.array_equal(arr2[5:10, 5:10], testarr2))

        self.assertTrue(abs(np.mean(arr3)-1.935546875) < 1e-09)
        self.assertTrue(np.array_equal(arr3[5:10, 5:10], testarr3))
        

    def test_tmask_border(self):
        """
        Main test for TMASK; runs the algorithm on a 32x32 pixel landsat8 time series
        and checks the results
        """
        cmdargs = self.Helper("border")
                    
        reflist, cloudlist, datelist, outlist = readInputs(cmdargs)
        tmask(reflist, cloudlist, datelist, outlist)

        # Let's check...
        # ...1. the number of created output files
        tmasklist = glob.glob('./testdata/border/tmask*.kea')
        self.assertEqual(len(tmasklist), 36)

        # 2. values of three output files        
        ds1 = gdal.Open(tmasklist[0])
        ds2 = gdal.Open(tmasklist[1])
        ds3 = gdal.Open(tmasklist[14])

        arr1 = np.array(ds1.GetRasterBand(1).ReadAsArray())
        arr2 = np.array(ds2.GetRasterBand(1).ReadAsArray())
        arr3 = np.array(ds3.GetRasterBand(1).ReadAsArray())

        self.assertTrue(abs(np.mean(arr1)) < 1e-09)
        self.assertTrue(abs(np.mean(arr2)) < 1e-09)
        self.assertTrue(abs(np.mean(arr3)) < 1e-09)
        

if __name__ == '__main__':
    unittest.main()

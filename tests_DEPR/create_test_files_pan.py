#!/usr/bin/env python
"""
Creates test files for running unit tests
"""
import sys
sys.path.append('./..')
from run_tmask_pan import *
import glob
import os
import argparse


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("testcase", type = str, help="one of standard, border")

    args = parser.parse_args()
    
    inpath = os.path.join(os.getenv('LCR_DATA'),'raster', 'landsat8')
    pathrow = 'p72r88'
    outpath = os.path.join('./testdata', args.testcase)

    cloudfiles = glob.glob('{}/*cloud*.kea'.format(outpath))
    cloudfiles.sort()
    toafiles = glob.glob('{}/*toa*.kea'.format(outpath))
    toafiles.sort()
    caldates = [cloud.split('_')[3] for cloud in cloudfiles]
    juldates = [date_to_julian_day(datetime.strptime(caldate, "%y%m%d")) for caldate in caldates]
    outfiles = ['./testdata/{}/tmask_{}.kea'.format(args.testcase, caldate) for caldate in caldates]

    cloudtxtfile = os.path.join(outpath, 'cloudfiles.txt')
    toatxtfile = os.path.join(outpath, 'reffiles.txt')
    datenumberstxtfile = os.path.join(outpath, 'datenumbers.txt')
    outfilestxtfile = os.path.join(outpath, 'outfiles.txt')

    with open(cloudtxtfile, 'w', encoding='utf-8') as cloud_file:
        [cloud_file.write(cloud+'\n') for cloud in cloudfiles]

    with open(toatxtfile, 'w', encoding='utf-8') as toa_file:
        [toa_file.write(toa+'\n') for toa in toafiles]

    with open(datenumberstxtfile, 'w', encoding='utf-8') as date_file:
        [date_file.write(str(juldate)+'\n') for juldate in juldates]

    with open(outfilestxtfile, 'w', encoding='utf-8') as out_file:
        [out_file.write(out+'\n') for out in outfiles]
        
    
    

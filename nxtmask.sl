#!/bin/bash

#SBATCH -A landcare00034
#SBATCH --time=05:59:00
#SBATCH --mem-per-cpu=40G
#SBATCH -o /projects/landcare00034/bentmp/logs/tmask.job-%j.out
#SBATCH -e /projects/landcare00034/bentmp/logs/tmask.job-%j.err
#SBATCH --job-name=tmask

source /landcare/sw/osgeo/osgeo.sh
envmaster load gsl python-fmask/0.4.0 python-nxtmask/dev


export RIOS_DFLT_JOBMGRTYPE=slurm
export RIOS_SLURMJOBMGR_SBATCHOPTIONS="-A landcare00034 --time=05:59:00 --mem-per-cpu=40G"
export RIOS_SLURMJOBMGR_INITCMDS="source /landcare/sw/osgeo/osgeo.sh; envmaster load gsl python-fmask/0.4.0 python-nxtmask/dev;export PYTHONPATH=$PYTHONPATH:/home/ben.jolly/envmaster/src/python-nxtmask/python-nxtmask-dev/python-nxtmask/"
c
# TMPDIR has to be a persistent directory, the sub-jobs will otherwise not find it
TMPDIR=/projects/landcare00034/bentmp/tmask_trials/tmp
mkdir -p $TMPDIR

echo TMPDIR=$TMPDIR
#cd $TMPDIR

#python /home/markus.mueller/projects/python-tmask/run_tmask_pan.py $1 $2
python nxtmask.py $@

#LOGFILE=/projects/landcare00034/mmtmp/tmask_trials/finished/$1
# Create file that indicates processing was properly finished and then delete temp dir
#touch $LOGFILE
#rm -r $TMPDIR



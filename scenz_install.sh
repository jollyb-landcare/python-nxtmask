#!/bin/bash

miniconda="$HOME/miniconda3"
if [ "$#" -eq 2 ]; then
    miniconda=$2
elif [ "$#" -ne 1 ]; then
    echo "Usage: ./scenz_install.sh /path/to/bin [/path/to/miniconda (if not $HOME/miniconda3)]"
    exit 1
fi

bin=$1

conda install numpy gsl pip

export GSL_LIB_PATH="$miniconda/lib/"
export GSL_INCLUDE_PATH="$miniconda/include/"
alias f2py='"$miniconda/bin/f2py"' 

export INSTALL_LCRFS="True"

echo "Executable script install location: "$bin
echo "conda location:  "$miniconda
echo GSL_LIB_PATH=$GSL_LIB_PATH
echo GSL_INCLUDE_PATH=$GSL_INCLUDE_PATH
echo "Set alias f2py="$miniconda/bin/f2py
echo INSTALL_LCRFS=$INSTALL_LCRFS
echo ""
echo "Installing using pip...."
echo ""
pip -v install . 
echo ""
echo "Installing executable scripts to "$bin

chmod +x bin/*.py
cp bin/* "$bin"

#!/bin/bash

echo "Suggested usage: ./envmaster_install.sh $JGEO_ROOT/local 1.0"

installroot=$1
version=$2

envmaster load gsl setuptools
 
prefix=$installroot/$LCRARCH/python-nxtmask/$version
module=$installroot/modules/python-nxtmask/$version

bin=$prefix/bin/

echo "Install location: "$prefix
echo "Module location:  "$module
echo ""

mkdir -p $(dirname "${module}")

python setup.py install --prefix=$prefix

echo "#%EnvMaster1.0

module.prereq('python')
module.prereq('numpy')
module.prereq('gdal')
module.prereq('rios')
module.prereq('gsl')

" > $module

echo "module.setAll('"$prefix"')" >> $module

echo "Installing executable scripts to "$bin

mkdir -p $bin

cp bin/* $bin

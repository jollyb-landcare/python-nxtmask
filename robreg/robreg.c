#include <stdio.h>
/*
#include <gsl/gsl_fit.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_cdf.h>
*/
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_errno.h>
#include <math.h>


/*  A wrapper around the GSL multi-variate robust regression routine.  
    
    It is intended that this routine should only ever be called from the Python
    wrapper function, so if I ever need to change the parameters, I will only need 
    to change that. 
    
    The array variables are given as pointers to doubles, and are referenced as
    1-d arrays. However, the indexes are calculated as though they were multi-dimensional
    arrays, and the dimensions are described below. 
    
    Input Variables
    ***************
   
    numParams is the number of parameters to be fitted, also equal 
        to the number of independant variables
    numImages is the number of images in the stack, and corresponds to the number
        of points in each fit (before removing nulls)
    numRows is the number of rows in the image
    numCols is the number of columns in the image

    The x parameter contains the values of all the independant variables.
    It comes in one of two flavours, depending on the value of the
    perPixelX parameter. If perPixelX is 1, then the x parameter is a 4-dimensional
    array ofdoubles, with shape
        (numParams, numImages, numRows, numCols)
    otherwise it is a 2-dimensional array with shape
        (numParams, numImages)
    and it is assuemd that the independant variables are constant over all pixels. This
    latter is the most likely case. 
    
    The Y variable is notionally a 3-dimensional array of doubles, 
    The shape should be
        (numImages, numRows, numCols)
    It contains the values of the dependent variable. 
   
    The method is an integer, and is translated to the corresponding GSL constants
    for different methods of calculating weights, as follows
        1 - gsl_multifit_robust_bisquare
        2 - gsl_multifit_robust_cauchy
        3 - gsl_multifit_robust_fair
        4 - gsl_multifit_robust_huber
        5 - gsl_multifit_robust_ols
        6 - gsl_multifit_robust_welsch
    
    The nullVal parameter is a scalar double value. Any occurrence of this value in the
    y array will exclude that point from the fit. 
    
    Output Variables
    ****************
    
    The following variables are calculated within this routine, and passed back via the
    pointers in the parameter list.  
    
    The c array is a 3-dimensional array of doubles. It corresponds to an image 
    stack of the fitted coefficients, and its shape is
        (numParams, numRows, numCols)
    
    The Rsqrd array stores the adjusted R^2 coefficient of determination
        statistic. 

*/
void wrap_gsl_multifit_robust(double *x, double *y, double *c, double *Rsqrd, 
        int *numIter, double *rmse, int method, int perPixelX, 
        int numRows, int numCols, int numImages, int numParams, 
        int numRowsX, int numColsX, double nullVal) {
    int row, col, img, param, n, xNdx, yNdx, pixNdx;
    gsl_matrix *gslX, *gslCov;
    gsl_vector *gslY, *gslC;
    gsl_multifit_robust_workspace *workspace;
    gsl_multifit_robust_type *regressionType;
    gsl_multifit_robust_stats stats;
    int gslErrorCode;
    
    /* Turn off the default error handler, which aborts atthe first error. */
    gsl_set_error_handler_off();
    
    /* Translate the integer type given into the corresponding GSL pointer */
    switch (method) {
        case 1: regressionType = gsl_multifit_robust_bisquare; break;
        case 2: regressionType = gsl_multifit_robust_cauchy; break;
        case 3: regressionType = gsl_multifit_robust_fair; break;
        case 4: regressionType = gsl_multifit_robust_huber; break;
        case 5: regressionType = gsl_multifit_robust_ols; break;
        case 6: regressionType = gsl_multifit_robust_welsch; break;
    }

    /* These structures can be allocated outside the pixel loop */
    gslCov = gsl_matrix_calloc(numParams, numParams);
    gslC = gsl_vector_calloc(numParams);
    
    /* Loop over all pixels */
    for (row=0; row<numRows; row++) {
        for (col=0; col<numCols; col++) {
            /* Count how many non-null y values we have. */
            n = 0;
            for (img=0; img<numImages; img++) {
                if (y[img*numRows*numCols+row*numCols+col] != nullVal) n++; 
            }
            
            /* Allocate various structures, now we know how many non-nulls. 
               Not sure how inefficient it is to do this for every pixel, but 
               the number of points is buried deep within these structures, 
               so we kind of need to. 
            */
            if (n >= numParams) {
                workspace = gsl_multifit_robust_alloc(regressionType, n, numParams);
                gslX = gsl_matrix_calloc(n, numParams);
                gslY = gsl_vector_calloc(n);

                /* Copy the data from this pixel into the relevant GSL structures. Note
                   that we skip over null values, based on nulls in the y variable. 
                */
                n = 0;
                for (img=0; img<numImages; img++) {
                    yNdx = img*numRows*numCols+row*numCols+col;
                    if (y[yNdx] != nullVal) {
                        gslY->data[n*gslY->stride] = y[yNdx];
                        for (param=0; param<numParams; param++) {
                            if (perPixelX == 0) {
                                xNdx = param * numImages + img;
                            } else {
                                xNdx = param * numRowsX * numColsX * numImages + 
                                    img * numRowsX * numColsX + row * numColsX + col;
                            }
                            gslX->data[n*gslX->tda + param] = x[xNdx];
                        }
                        n++;
                    }
                }

                /* Do the regression fit */
                gslErrorCode = gsl_multifit_robust(gslX, gslY, gslC, gslCov, workspace);

                if (gslErrorCode == 0) {
                    /* Copy the coefficients back into the image stack of coefficients */
                    for (param=0; param<numParams; param++) {
                        c[param*numRows*numCols + row*numCols + col] = gslC->data[param*gslC->stride];
                    }

                    /* Copy some useful statistics into their arrays */
                    stats = gsl_multifit_robust_statistics(workspace);
                    pixNdx = row*numCols + col;
                    Rsqrd[pixNdx] = stats.Rsq;
                    numIter[pixNdx] = stats.numit;
                    rmse[pixNdx] = stats.rmse;
                }
                /*else {
                    printf ("error: %s\n", gsl_strerror (gslErrorCode));
                }*/

                /* Free per-pixel structures */
                gsl_matrix_free(gslX);
                gsl_vector_free(gslY);
                gsl_multifit_robust_free(workspace);
            }
        }
    }
    
    
    /* Free the other structures */
    gsl_vector_free(gslC);
    gsl_matrix_free(gslCov);
}

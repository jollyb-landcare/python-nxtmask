#python-nxtmask

A modification of the tmask algorithm implementation by Neil Flood for cloud masking, based on work by Markus Mueller.

###From Markus:
```
- Runs on a three year set of imagery, as per the original paper. 
- Requires GNU Scientific Library to be installed (fairly recent version
  required - I am using version 1.16). 
- To build the robust regression piece, use the Makefile
    make robreg
- The files robustregression.py and robreg.so must be place in a directory which
  is in the PYTHONPATH. The RIOS parallel processing stuff needs this to be the
  case so that sub-jobs will be able to find them. 
- Assumes input TOA reflectances are a single stack of the standard TM bands 1-6.
  So, caution is required with Landsat-8 and its extra band. 
- Assumes input TOA reflectances are scaled as reflectance*10000, with null value
  of 32767
- Assumes input Fmask layers are a single layer, with coding:
    null = 0
    clear = 1
    cloud = 2
    shadow = 3
    snow = 4
    water = 5
- Blocks size if set to 512 x 512 pixels. If using numthreads>1, assumes you are
  set up to use RIOS parallel processing. Each job requires around 2Gb of memory,
  and takes around 5 minutes. I had good success with "--numthreads 30", and full
  run took around 90 minutes. With only a single thread it is on the order of 24
  hours. 
- Output is a single layer, coded as per the input (no codes for snow or water). 
- File lists and date list for commandline MUST be sorted into date order
  (earliest first). 
```

